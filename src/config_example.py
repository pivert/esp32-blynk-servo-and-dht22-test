# Configuration file
# Adapt to your needs, and rename the file to config.py
AuthToken = 'xxxxxxxx_xxxxxxx_xxxxx-xxx-xxxxx'
WIFI_SSID = 'wifissid'
WIFI_PASS = 'wifipass'
DHT22_PIN = 2
SERVO_PIN = 27
SERVO_MAX_ANGLE = 160
