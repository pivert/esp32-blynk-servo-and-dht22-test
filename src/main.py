"""
    main.py - The core software

    Test program to check environment sensors, manage a servo, and communicate to smartphone via Blynk
    Tested on TTGO T-Display based on ESP32 with MicroPython 1.12 firmware compiled from sources with
    LCD color screen support. (st7789 driver from https://github.com/devbis/st7789_mpy)

    Copyright (C) 2020 François Delpierre

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


.. todolist::
  - Add fonts & text display or recompile with : https://github.com/russhughes/st7789_mpy
  - Upgrade to MicroPython 1.13
"""


import config
import os
import gc
import sys
from time import sleep, ticks_ms

import BlynkLib
import dht
import ntptime
from machine import PWM, RTC, Pin, Timer, reset


# Variables & other init:
gravity_enabled = False
print_line_format = "{0:04d}-{1:02d}-{2:02d} {4:02d}:{5:02d}:{6:02d}Z  Temperature : {t:02.1f}°C  Humidity : {h:02.1f}%"

def servo_angle(angle):
    """Convert angle into PWM ratio for the servo"""
    return int(angle/config.SERVO_MAX_ANGLE * (115 - 40)) + 40

# Initialize Blynk
print("Connecting to Blynk...")
try:
    blynk = BlynkLib.Blynk(config.AuthToken)
except ValueError:
    print("ValueError: reset in 5s")
    sleep(5)
    reset()


@blynk.on('readV2')
def v2_read_handler():
    """uptime in minutes since last reset """
    blynk.virtual_write(2, ticks_ms() // 1000 // 60)

@blynk.on('readV4')
def v4_read_handler():
    """Get free memory from garbage collector"""
    blynk.virtual_write(4, gc.mem_free() // 1024)

@blynk.on('V1')
def servo_handler(value):
    """Servo simple command"""
    print("New servo value: {}".format(value[0]))
    val = servo_angle(int(value[0]))
    servo0.duty(val)

def measure_env(t):
    """Fuction triggered periodically by timer to measure & send data from environment sensor"""
    d.measure()
    now = rtc.datetime()[0:7]
    temperature, humidity = round(d.temperature(), 1), round(d.humidity(),1) 
    print(print_line_format.format( *now, t=temperature, h=humidity))
    try:
        blynk.virtual_write(0, temperature)
        blynk.virtual_write(3, humidity)
    except OSError:
        print("Blynk or WiFi probably disconnected. Resetting in 5s.")
        sleep(5)
        reset()
        
if __name__ == '__main__':
    # set time from network NTP servers
    rtc = RTC()
    ntptime.settime()

    # Initialize Servo0
    servo0 = PWM(Pin(config.SERVO_PIN), freq=50)

    # Initialize DHT
    d = dht.DHT22(Pin(config.DHT22_PIN))
    d.measure()

    # Initialize timer0
    tim0 = Timer(0)
    tim0.init(period=10000, mode=Timer.PERIODIC, callback=measure_env)

    print("Starting Blynk")
    while True:
        try:
            blynk.run()
        except KeyboardInterrupt:
            # Stop the timer before exit
            tim0.deinit()
            raise KeyboardInterrupt
